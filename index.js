// console.log("Hello Good Afternoon")

// Functions
    // are lines/block of codes that tell oyr devices/application to perform a certain task when called/invoke.
    // ARe mostly created to create complicated task to run several lines of codes in succession.
    // They are also used to prevent repeating lines/blocks of codes that contains the same task/functions.

    // Learned in the previous session that we can gather data from user using prompt windows.

        // function printInput(){
        //     let nickname = prompt("Enter your nickname: ")
        //     console.log("Hi " + nickname);
        // }

        // printInput();

    // However, for sone yse cases, this may not be ideal
    // For some case, function can be also process dara directly [assed into it ] instead of relying on global variable and prompt.

// Parameters and Arguements

        // conside this function
        function printName(name){
            console.log("My name is " + name);
        }

        printName("Chris");
        printName("George")

        // You can directly pass data into the function. The function can then use the data which is refered as "name" within the function.

        // "name" is called parameter.
        // parameter acts as named variable/contaimer that exist only inside of a function.

        // "Chris ", acts the information/data provided directly into the function called argument.
        // Values passed when invokung a function are called arguements. These arguementrs are then stored as the parameters within the function.

    // variable can also be passed as an arguement.
        let sampleVariable = "Edward";
        printName(sampleVariable);
    
    // function arguements cannot be used by a function i there are no parametres provided withn the function.

        function noParams(){
            let params = "No parameter";

            console.log(params);
        }

        noParams("With pamater");

    function checkDivisibilityBy8(num){
        let modulo = num%8;
        console.log("The remainder of " + num + " divided by 8 is: " + modulo);

        let isDivisibleBy8 = modulo === 0;
        console.log("Is " + num + " divisible by 8?")
        console.log(isDivisibleBy8);
    }

    checkDivisibilityBy8(8);

    checkDivisibilityBy8(17);

    // can also the sanme using yhe prompt(), however, take note that prompt() outputs a string. Srings are not ideal for mathematical computations.


    // Functions as Arguments
        // function parameters can laso accept other functions as arguments.
        // Some complex funtions use other functiond sd srguments to perform more complicated results.
        // Thus will be further seen when we discuss arrays methods.

        function argumentFunction(){
            console.log("This function was passed as an afrgunemt messafe was printed.");
        }

        function invokeFunction(argFunction){
            argFunction();
        }

        invokeFunction(argumentFunction);

        // Adding and removing the paranthesis "()" impacts the output of Javascript heavily.
        // When function is used with "()", it denotes invokuing a finction.
        // A function used without parethesis "()" is normally associated with using the function as an argument to another function.

    // Using multiple parameters
        // Multple "arguments" will correspond to the number of "parameters" declared in a function in "successding order".

        function createFullName(firstName, middleName, lastName){
            console.log("This is firsName: " + firstName);
            console.log("This is middleName: " + middleName);
            console.log("This is lastName: " + lastName);
        }
        
        createFullName("Juan", "Dela", "Cruz");

        // Juan will be stored in the parameter "firstName"
        // Dela will be store in the paramete "middleName"
        // Cruz will be stored in the parameter "lastName"

        // in JS, provideing more  argumenrs than the expected parameters will not return an error
        createFullName("Juan", "Dela", "Cruz", "Jr.");

        //
        createFullName("Juan", undefined ,"Dela Cruz");

        // Uisng Variable as arguments.

        let firstName = "John";
        let middleName = "Doe";
        let lastName = "Smith";

        createFullName(firstName, middleName, lastName);

    // Return statement
        // "return" statement allows us to output a value from a function to be passed to the line/block of code that invoked the function.

        function returnFullName(firstName, middleName, lastName){
            console.log(firstName + " " + middleName + " " + lastName)

        }

        returnFullName("Ada", "None" , "Lovelace");

        function returnFullName (firsName, middleName, lastName){
            return firstName + " " + middleName + " " + lastName;

            console.log(firsName);

        }

        console.log(returnFullName("John", "Doe", "Smith"))

        let fullName = returnFullName("John", "Doe", "Smith");
        console.log("This is the console.log from fullName variable:");
        console.log(fullName);

        function printPlayerInfo(userName, level, job){
            console.log("Username: " + userName);
            console.log("Level: " + level);
            console.log("Job: " + job);

            return userName + " " + level + " " + job;
        }

        printPlayerInfo("knight_white", 95, "Paladin");

        let user1 = printPlayerInfo("knight_white", 95, "Paladin");
        console.log(user1);
